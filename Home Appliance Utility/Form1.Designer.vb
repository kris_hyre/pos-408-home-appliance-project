﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lv_appliance_costs = New System.Windows.Forms.ListView()
        Me.clm_appliance = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.clm_hours_used = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.clm_cost = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.cmb_appliances = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lbl_gal_hr_used = New System.Windows.Forms.Label()
        Me.btn_exit = New System.Windows.Forms.Button()
        Me.txt_hrs_used = New System.Windows.Forms.TextBox()
        Me.txt_kw_hr_used = New System.Windows.Forms.TextBox()
        Me.txt_gal_hr_used = New System.Windows.Forms.TextBox()
        Me.txt_elec_cost = New System.Windows.Forms.TextBox()
        Me.txt_water_cost = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.lbl_total_costs = New System.Windows.Forms.Label()
        Me.btn_export = New System.Windows.Forms.Button()
        Me.btn_remove_item = New System.Windows.Forms.Button()
        Me.lbl_monthly = New System.Windows.Forms.Label()
        Me.lbl_monthly_cost = New System.Windows.Forms.Label()
        Me.lbl_yearly = New System.Windows.Forms.Label()
        Me.lbl_yearly_cost = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lv_appliance_costs
        '
        Me.lv_appliance_costs.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.clm_appliance, Me.clm_hours_used, Me.clm_cost})
        Me.lv_appliance_costs.Location = New System.Drawing.Point(421, 45)
        Me.lv_appliance_costs.Name = "lv_appliance_costs"
        Me.lv_appliance_costs.Size = New System.Drawing.Size(363, 336)
        Me.lv_appliance_costs.TabIndex = 0
        Me.lv_appliance_costs.UseCompatibleStateImageBehavior = False
        Me.lv_appliance_costs.View = System.Windows.Forms.View.Details
        '
        'clm_appliance
        '
        Me.clm_appliance.Text = "Appliance"
        Me.clm_appliance.Width = 120
        '
        'clm_hours_used
        '
        Me.clm_hours_used.Text = "Hours"
        Me.clm_hours_used.Width = 120
        '
        'clm_cost
        '
        Me.clm_cost.Text = "Cost"
        Me.clm_cost.Width = 120
        '
        'cmb_appliances
        '
        Me.cmb_appliances.FormattingEnabled = True
        Me.cmb_appliances.Items.AddRange(New Object() {"Laundry Washer", "DVD Player", "4k TV", "Space Heather", "Computer w/ Monitor", "Oven", "Air Conditioner"})
        Me.cmb_appliances.Location = New System.Drawing.Point(181, 163)
        Me.cmb_appliances.Name = "cmb_appliances"
        Me.cmb_appliances.Size = New System.Drawing.Size(121, 21)
        Me.cmb_appliances.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(32, 48)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(104, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Electric Price per kw"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(32, 211)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(63, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "kw/hr Used"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(32, 93)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(98, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Water Price per gal"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(32, 298)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(59, 13)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "hours used"
        '
        'lbl_gal_hr_used
        '
        Me.lbl_gal_hr_used.AutoSize = True
        Me.lbl_gal_hr_used.Enabled = False
        Me.lbl_gal_hr_used.Location = New System.Drawing.Point(32, 256)
        Me.lbl_gal_hr_used.Name = "lbl_gal_hr_used"
        Me.lbl_gal_hr_used.Size = New System.Drawing.Size(61, 13)
        Me.lbl_gal_hr_used.TabIndex = 6
        Me.lbl_gal_hr_used.Text = "gal/hr used"
        '
        'btn_exit
        '
        Me.btn_exit.Location = New System.Drawing.Point(804, 447)
        Me.btn_exit.Name = "btn_exit"
        Me.btn_exit.Size = New System.Drawing.Size(75, 42)
        Me.btn_exit.TabIndex = 7
        Me.btn_exit.Text = "Save and Exit"
        Me.btn_exit.UseVisualStyleBackColor = True
        '
        'txt_hrs_used
        '
        Me.txt_hrs_used.Location = New System.Drawing.Point(181, 298)
        Me.txt_hrs_used.Name = "txt_hrs_used"
        Me.txt_hrs_used.Size = New System.Drawing.Size(100, 20)
        Me.txt_hrs_used.TabIndex = 6
        Me.txt_hrs_used.Text = "0"
        '
        'txt_kw_hr_used
        '
        Me.txt_kw_hr_used.Location = New System.Drawing.Point(181, 211)
        Me.txt_kw_hr_used.Name = "txt_kw_hr_used"
        Me.txt_kw_hr_used.Size = New System.Drawing.Size(100, 20)
        Me.txt_kw_hr_used.TabIndex = 4
        Me.txt_kw_hr_used.Text = "0"
        '
        'txt_gal_hr_used
        '
        Me.txt_gal_hr_used.Enabled = False
        Me.txt_gal_hr_used.Location = New System.Drawing.Point(181, 249)
        Me.txt_gal_hr_used.Name = "txt_gal_hr_used"
        Me.txt_gal_hr_used.Size = New System.Drawing.Size(100, 20)
        Me.txt_gal_hr_used.TabIndex = 5
        Me.txt_gal_hr_used.Text = "0"
        '
        'txt_elec_cost
        '
        Me.txt_elec_cost.Location = New System.Drawing.Point(181, 45)
        Me.txt_elec_cost.Name = "txt_elec_cost"
        Me.txt_elec_cost.Size = New System.Drawing.Size(100, 20)
        Me.txt_elec_cost.TabIndex = 1
        Me.txt_elec_cost.Text = "0.10"
        '
        'txt_water_cost
        '
        Me.txt_water_cost.Location = New System.Drawing.Point(181, 93)
        Me.txt_water_cost.Name = "txt_water_cost"
        Me.txt_water_cost.Size = New System.Drawing.Size(100, 20)
        Me.txt_water_cost.TabIndex = 2
        Me.txt_water_cost.Text = "0.10"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(32, 163)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(102, 13)
        Me.Label6.TabIndex = 14
        Me.Label6.Text = "Chose an Appliance"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(418, 409)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(60, 13)
        Me.Label7.TabIndex = 15
        Me.Label7.Text = "Total Costs"
        '
        'lbl_total_costs
        '
        Me.lbl_total_costs.AutoSize = True
        Me.lbl_total_costs.Location = New System.Drawing.Point(503, 409)
        Me.lbl_total_costs.Name = "lbl_total_costs"
        Me.lbl_total_costs.Size = New System.Drawing.Size(28, 13)
        Me.lbl_total_costs.TabIndex = 16
        Me.lbl_total_costs.Text = "0.00"
        '
        'btn_export
        '
        Me.btn_export.Location = New System.Drawing.Point(568, 445)
        Me.btn_export.Name = "btn_export"
        Me.btn_export.Size = New System.Drawing.Size(97, 44)
        Me.btn_export.TabIndex = 17
        Me.btn_export.Text = "Export List"
        Me.btn_export.UseVisualStyleBackColor = True
        '
        'btn_remove_item
        '
        Me.btn_remove_item.Location = New System.Drawing.Point(804, 67)
        Me.btn_remove_item.Name = "btn_remove_item"
        Me.btn_remove_item.Size = New System.Drawing.Size(75, 39)
        Me.btn_remove_item.TabIndex = 18
        Me.btn_remove_item.Text = "Remove Item"
        Me.btn_remove_item.UseVisualStyleBackColor = True
        '
        'lbl_monthly
        '
        Me.lbl_monthly.AutoSize = True
        Me.lbl_monthly.Location = New System.Drawing.Point(418, 445)
        Me.lbl_monthly.Name = "lbl_monthly"
        Me.lbl_monthly.Size = New System.Drawing.Size(44, 13)
        Me.lbl_monthly.TabIndex = 19
        Me.lbl_monthly.Text = "Monthly"
        '
        'lbl_monthly_cost
        '
        Me.lbl_monthly_cost.AutoSize = True
        Me.lbl_monthly_cost.Location = New System.Drawing.Point(503, 447)
        Me.lbl_monthly_cost.Name = "lbl_monthly_cost"
        Me.lbl_monthly_cost.Size = New System.Drawing.Size(28, 13)
        Me.lbl_monthly_cost.TabIndex = 20
        Me.lbl_monthly_cost.Text = "0.00"
        '
        'lbl_yearly
        '
        Me.lbl_yearly.AutoSize = True
        Me.lbl_yearly.Location = New System.Drawing.Point(418, 476)
        Me.lbl_yearly.Name = "lbl_yearly"
        Me.lbl_yearly.Size = New System.Drawing.Size(36, 13)
        Me.lbl_yearly.TabIndex = 21
        Me.lbl_yearly.Text = "Yearly"
        '
        'lbl_yearly_cost
        '
        Me.lbl_yearly_cost.AutoSize = True
        Me.lbl_yearly_cost.Location = New System.Drawing.Point(503, 476)
        Me.lbl_yearly_cost.Name = "lbl_yearly_cost"
        Me.lbl_yearly_cost.Size = New System.Drawing.Size(28, 13)
        Me.lbl_yearly_cost.TabIndex = 22
        Me.lbl_yearly_cost.Text = "0.00"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(919, 517)
        Me.Controls.Add(Me.lbl_yearly_cost)
        Me.Controls.Add(Me.lbl_yearly)
        Me.Controls.Add(Me.lbl_monthly_cost)
        Me.Controls.Add(Me.lbl_monthly)
        Me.Controls.Add(Me.btn_remove_item)
        Me.Controls.Add(Me.btn_export)
        Me.Controls.Add(Me.lbl_total_costs)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txt_water_cost)
        Me.Controls.Add(Me.txt_elec_cost)
        Me.Controls.Add(Me.txt_gal_hr_used)
        Me.Controls.Add(Me.txt_kw_hr_used)
        Me.Controls.Add(Me.txt_hrs_used)
        Me.Controls.Add(Me.btn_exit)
        Me.Controls.Add(Me.lbl_gal_hr_used)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cmb_appliances)
        Me.Controls.Add(Me.lv_appliance_costs)
        Me.Name = "Form1"
        Me.Text = "Home Appliance Utility"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lv_appliance_costs As ListView
    Friend WithEvents cmb_appliances As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents lbl_gal_hr_used As Label
    Friend WithEvents btn_exit As Button
    Friend WithEvents txt_hrs_used As TextBox
    Friend WithEvents txt_kw_hr_used As TextBox
    Friend WithEvents txt_gal_hr_used As TextBox
    Friend WithEvents txt_elec_cost As TextBox
    Friend WithEvents txt_water_cost As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents clm_appliance As ColumnHeader
    Friend WithEvents clm_hours_used As ColumnHeader
    Friend WithEvents clm_cost As ColumnHeader
    Friend WithEvents Label7 As Label
    Friend WithEvents lbl_total_costs As Label
    Friend WithEvents btn_export As System.Windows.Forms.Button
    Friend WithEvents btn_remove_item As System.Windows.Forms.Button
    Friend WithEvents lbl_monthly As Label
    Friend WithEvents lbl_monthly_cost As Label
    Friend WithEvents lbl_yearly As Label
    Friend WithEvents lbl_yearly_cost As Label
End Class
