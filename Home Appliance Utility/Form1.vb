﻿' Title: Home Utility Application
' Version: 0.4.2
' Author: Kris Hyre
' Date: Nov. 27, 2016
' Class: POS/408 .NET I
' Instructor: Paul Stay
' Description: This application will allow the user to add various appliances, the cost in electricity
'  and water, the amount of time used and calucate the total cost to operate them.

Imports System.IO

Public Class Form1

    'Variables used for some error messages
    Dim not_decimal As String = "Please enter a numeric value greater than 0.00"
    Dim not_price As String = "Please enter a numric value between 0.10 and 0.56"

    ' Change flags used to determine if the listview is up to date
    Dim is_new_item As Boolean = False
    Dim are_hours_updated As Boolean = False
    Dim are_kw_hr_updated As Boolean = False
    Dim are_gal_hr_updated As Boolean = False

    Dim final_total_cost As Decimal = 0D

    'Dictionary used to hold values for default electricity usage for appliances
    Dim appliances As Dictionary(Of Integer, String) = New Dictionary(Of Integer, String)

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ' Files for default prices
        Dim price_file_out As StreamWriter
        Dim price_file_in As StreamReader
        Dim default_price = "default_price.txt"

        ' Check and see if there is a file for the default vales, if there is not, make one.
        ' If there is, set the price values
        If File.Exists(default_price) Then
            price_file_in = File.OpenText(default_price)
            txt_elec_cost.Text = price_file_in.ReadLine()
            txt_water_cost.Text = price_file_in.ReadLine()
            price_file_in.Close()
        Else
            price_file_out = File.CreateText(default_price)
            price_file_out.WriteLine("0.10")
            price_file_out.WriteLine("0.10")
            price_file_out.Close()
        End If

        Dim power_file_out As StreamWriter
        Dim power_file_in As StreamReader
        Dim default_power_ratings = "default_power_ratings.txt"

        If File.Exists(default_power_ratings) Then
            ' reads the existing file, takes each line, and enters the values into the appliances dictionary
            power_file_in = File.OpenText(default_power_ratings)
            Do While power_file_in.Peek <> -1
                Dim entry As String() = power_file_in.ReadLine().Split(","c)
                If entry.Length = 2 Then
                    Dim entry_key_clean = entry(0).Replace("[", "")
                    Dim entry_value_clean = entry(1).Replace("]", "")
                    Dim entry_key_int = Integer.Parse(entry_key_clean)
                    appliances.Add(entry_key_int, entry_value_clean)
                End If
            Loop
            power_file_in.Close()

        Else
            ' Otherwise, create a new file and dictionary with default entries
            power_file_out = File.CreateText(default_power_ratings)
            power_file_out.WriteLine("0,0.00")
            appliances.Add(0, "0.00")
            power_file_out.WriteLine("1,0.00")
            appliances.Add(1, "0.00")
            power_file_out.WriteLine("2,0.00")
            appliances.Add(2, "0.00")
            power_file_out.WriteLine("3,0.00")
            appliances.Add(3, "0.00")
            power_file_out.WriteLine("4,0.00")
            appliances.Add(4, "0.00")
            power_file_out.WriteLine("5,0.00")
            appliances.Add(5, "0.00")
            power_file_out.WriteLine("6,0.00")
            appliances.Add(6, "0.00")
            power_file_out.Close()
        End If

    End Sub

    Private Function validate_entry(value As String) As Boolean
        ' Check if the entered value is a valid decimal
        Dim entered_value = value
        Dim num_value As Decimal
        Return Decimal.TryParse(entered_value, num_value)
    End Function

    Private Function validate_price(value As String) As Boolean

        Dim entered_value = value
        Dim price_value As Decimal

        ' Takes the entered value and tests to see if it is a valid decimal value
        ' and if it is between the required values of 0.10 and 0.56, and returns True or False

        Try
            price_value = Decimal.Parse(entered_value)

            If (price_value < 0.1) <> (price_value > 0.56) Then
                MessageBox.Show(not_price)
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            MessageBox.Show(not_decimal)
            Return False
        End Try

    End Function

    Private Sub btn_exit_Click(sender As Object, e As EventArgs) Handles btn_exit.Click
        ' Save price values
        update_prices()

        ' Save power values
        update_default_power()

        ' Exit the application
        Me.Close()

    End Sub

    Private Sub txt_elec_cost_TextChanged(sender As Object, e As EventArgs) Handles txt_elec_cost.TextChanged
        ' Check the changed value to see if valid price between 0.10 and 0.56
        ' If not, set it to the default 0.10 value
        Dim is_price = validate_price(txt_elec_cost.Text)
        If Not is_price Then
            txt_elec_cost.Text = "0.10"
        End If

        Dim rounded As String = round_off(txt_elec_cost.Text).ToString
        txt_elec_cost.Text = rounded

    End Sub

    Private Sub txt_water_cost_TextChanged(sender As Object, e As EventArgs) Handles txt_water_cost.TextChanged
        ' Check the changed value to see if valid price between 0.10 and 0.56
        ' If not, set it to the default 0.10 value
        Dim is_price = validate_price(txt_water_cost.Text)
        If Not is_price Then
            txt_water_cost.Text = "0.10"
        End If

        Dim rounded As String = round_off(txt_water_cost.Text).ToString
        txt_water_cost.Text = rounded

    End Sub

    Private Sub cmb_appliances_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmb_appliances.SelectedIndexChanged
        ' If the laundry washer is selected, enable the fields for water data
        If cmb_appliances.SelectedIndex = 0 Then
            lbl_gal_hr_used.Enabled = True
            txt_gal_hr_used.Enabled = True
        Else
            lbl_gal_hr_used.Enabled = False
            txt_gal_hr_used.Enabled = False
        End If

        ' Pull default power data from the text file
        If File.Exists("default_power_ratings.txt") Then
            txt_kw_hr_used.Text = appliances(cmb_appliances.SelectedIndex).Replace(" ", "")
        End If

        ' Adjust the update flags 
        is_new_item = True
        are_hours_updated = False
        are_gal_hr_updated = False

        update_the_list()

    End Sub

    Private Sub txt_kw_hr_used_TextChanged(sender As Object, e As EventArgs) Handles txt_kw_hr_used.TextChanged
        ' If the kw/hr value has changed, check to see if it is valid
        ' If it is, check to see if the list needs to be updated
        Dim is_valid As Boolean = validate_entry(txt_kw_hr_used.Text)
        If Not is_valid Then
            MessageBox.Show(not_decimal)
            txt_kw_hr_used.Text = "0.00"
        Else
            are_kw_hr_updated = True
        End If

        update_the_list()

    End Sub

    Private Sub txt_gal_hr_used_TextChanged(sender As Object, e As EventArgs) Handles txt_gal_hr_used.TextChanged
        ' If the gallons/hr has changed, check to see if it is valid
        ' If it is, check to see if the list needs to be updated
        Dim is_valid As Boolean = validate_entry(txt_gal_hr_used.Text)
        If Not is_valid Then
            MessageBox.Show(not_decimal)
            txt_gal_hr_used.Text = "0.00"
        Else
            are_gal_hr_updated = True
        End If

        update_the_list()

    End Sub

    Private Sub txt_hrs_used_TextChanged(sender As Object, e As EventArgs) Handles txt_hrs_used.TextChanged
        ' If the hours have been updated, check to see if it is valid
        ' IF it is, check to see if the list needs to be updated
        Dim is_valid As Boolean = validate_entry(txt_hrs_used.Text)
        If Not is_valid Then
            MessageBox.Show(not_decimal)
            txt_hrs_used.Text = "0.00"
        Else
            are_hours_updated = True
        End If

        update_the_list()
    End Sub

    Private Sub update_prices()
        ' Grab the values from the text controls and write it to file
        Dim elec_price = txt_elec_cost.Text
        Dim water_price = txt_water_cost.Text
        Dim price_file_out As StreamWriter
        Dim default_price = "default_price.txt"

        File.Delete(default_price)
        price_file_out = File.CreateText(default_price)
        price_file_out.WriteLine(elec_price)
        price_file_out.WriteLine(water_price)
        price_file_out.Close()
    End Sub

    Private Sub update_the_list()
        ' Grab values from the text controls and convert to decimal for later math.
        Dim elec_price As Decimal
        Decimal.TryParse(txt_elec_cost.Text, elec_price)

        Dim water_price As Decimal
        Decimal.TryParse(txt_water_cost.Text, water_price)

        Dim hours As Decimal
        Decimal.TryParse(txt_hrs_used.Text, hours)

        Dim elec_used As Decimal
        Decimal.TryParse(txt_kw_hr_used.Text, elec_used)

        Dim water_used As Decimal
        Decimal.TryParse(txt_gal_hr_used.Text, water_used)

        Dim total_appliance_cost As Decimal
        Dim total_elec_price As Decimal
        Dim total_water_price As Decimal

        ' Check and see if we are using water with the appliance
        ' If not, just give a value of 0 for water.
        ' Otherwise calculate water costs
        If Not txt_gal_hr_used.Enabled Then
            are_gal_hr_updated = True
            total_water_price = 0D
        Else
            total_water_price = water_price * water_used * hours
            total_water_price = round_off(total_water_price.ToString)
        End If

        ' Figure out the total cost of running the appliance
        total_elec_price = elec_price * elec_used * hours
        total_elec_price = round_off(total_elec_price.ToString)

        total_appliance_cost = total_elec_price + total_water_price

        ' If all applicable values have been updated, grab the name of the appliance
        ' and the total costs and send that to the list view control.
        If is_new_item Then
            If are_kw_hr_updated Then
                If are_hours_updated Then
                    If are_gal_hr_updated Then
                        ' If everything has been updated, add the values to the listview
                        Dim new_listing As New ListViewItem(cmb_appliances.Text)
                        new_listing.SubItems.Add(hours.ToString)
                        new_listing.SubItems.Add(total_appliance_cost.ToString)

                        lv_appliance_costs.Items.Add(new_listing)

                        ' total the list costs
                        final_total_cost += total_appliance_cost
                        lbl_total_costs.Text = final_total_cost.ToString

                        calc_monthly_costs()
                        calc_yearly_costs()

                        ' then reset the change flags
                        is_new_item = False
                        are_gal_hr_updated = False
                        are_hours_updated = False
                        are_kw_hr_updated = False
                    End If
                End If
            End If
        End If

        ' Update the power usage in the appliance dict
        appliances(cmb_appliances.SelectedIndex) = txt_kw_hr_used.Text.Replace(" ", "")

    End Sub

    Private Sub update_default_power()
        ' Save the appliance dictionary to file.
        Dim power_file_out As StreamWriter
        Dim default_power_ratings = "default_power_ratings.txt"

        File.Delete(default_power_ratings)
        power_file_out = File.CreateText(default_power_ratings)
        For Each item In appliances
            If item.Key <> -1 Then
                ' Write the dictionary key & values, but get rid of that pesky blank space, first.
                power_file_out.WriteLine(item.Key.ToString + "," + item.Value.ToString.Replace(" ", ""))
            End If
        Next
        power_file_out.Close()

    End Sub

    Private Sub btn_remove_item_Click(sender As Object, e As EventArgs) Handles btn_remove_item.Click
        ' Delete the selected appliance and values from the list view and remove the cost from the total
        Dim items = lv_appliance_costs.SelectedItems
        Dim price = Decimal.Parse(lv_appliance_costs.SelectedItems(0).SubItems(2).Text)

        final_total_cost = final_total_cost - price
        lbl_total_costs.Text = final_total_cost.ToString

        calc_monthly_costs()
        calc_yearly_costs()

        lv_appliance_costs.Items.Remove(items(0))


    End Sub

    Private Sub btn_export_Click(sender As Object, e As EventArgs) Handles btn_export.Click
        ' This erases any existing utility list file and creates a new one with a CSV format of all items in the listview
        Dim utility_file_out As StreamWriter
        Dim utility_file = "utility_file.txt"

        If File.Exists(utility_file) Then
            File.Delete(utility_file)
        End If

        utility_file_out = File.CreateText(utility_file)

        Dim i As Integer = 0
        Dim output_string As String = ""

        ' Export the contents of the listview as CSV
        For i = 0 To lv_appliance_costs.Items.Count - 1 Step 1
            output_string = lv_appliance_costs.Items(i).SubItems(0).Text + "," + lv_appliance_costs.Items(i).SubItems(1).Text + "," + lv_appliance_costs.Items(i).SubItems(2).Text
            utility_file_out.WriteLine(output_string)
            output_string = ""
        Next

        ' Export the daily, monthly, and yearly totals
        output_string = "Daily," + lbl_total_costs.Text
        utility_file_out.WriteLine(output_string)
        output_string = "Monthly," + lbl_monthly_cost.Text
        utility_file_out.WriteLine(output_string)
        output_string = "Yearly, " + lbl_yearly_cost.Text
        utility_file_out.WriteLine(output_string)
        output_string = ""

        utility_file_out.Close()

    End Sub

    Private Sub calc_monthly_costs()
        ' Calucalte the monthly total based on a 30 day month
        Dim day As Decimal = Decimal.Parse(lbl_total_costs.Text)
        Dim monthly As Decimal = day * 30

        lbl_monthly_cost.Text = monthly.ToString

    End Sub

    Private Sub calc_yearly_costs()
        ' Calculate the yearly total based on 365 days
        Dim day As Decimal = Decimal.Parse(lbl_total_costs.Text)
        Dim yearly As Decimal = day * 365

        lbl_yearly_cost.Text = yearly.ToString

    End Sub

    Private Function round_off(value As String) As Decimal
        ' Rounding off strings to two decimal digits because I don't like to repeat myself
        Dim dec_value As Decimal = Decimal.Parse(value)
        Dim rounded_value = Math.Round(dec_value, 2)
        Return rounded_value

    End Function
End Class